// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VModal from 'vue-js-modal'
import VueMq from 'vue-mq'

Vue.use(VModal)
Vue.use(VueMq, {
  breakpoints: {
    mobile: 550,
    tablet: 900,
    laptop: 1250,
    desktop: Infinity
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#calendar',
  components: { App },
  template: '<App/>'
})
