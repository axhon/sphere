export default (range, day, doc) => {
  let tmp
  let toggled = range.map(item => {
    tmp = doc.getElementById(`${day}${item}`)
    tmp.checked = true
    return tmp
  })
  return toggled
}
