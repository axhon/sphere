import { shallow, mount } from 'vue-test-utils'
import DayModal from '@/components/DayModal'
import time from '@/TimeD'
import toggleElements from '@/ToggleElements'

describe('testing day-modal', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(DayModal, {
      props: {
        name: 'mon' + time[23],
        index: 23
      }
    })
    wrapper.setProps({
      name: 'mon' + time[23],
      index: 23
    })
  })
  afterEach(() => {
    wrapper.destroy()
  })
  describe('props and data', () => {
    it('name', () => {
      expect(wrapper.props().name).toBe('mon' + time[23])
    })
    it('index', () => {
      expect(wrapper.props().index).toBe(23)
    })
  })

  describe('actions', () => {
    describe('range selected', () => {
      it('mounts correctly', () => {
        wrapper.destroy()
        wrapper = mount(DayModal)
        expect(wrapper.contains('.modal-content')).toBe(true)
      })
    })

    describe('select range', () => {
      it('displays correct amount of options', () => {
        wrapper.destroy()
        wrapper = mount(DayModal, {
          propsData: {
            name: 'mon' + time[23],
            index: 23
          }
        })
        // all available options + 1 disabled
        let options = wrapper.findAll('.option')
        let range = wrapper.vm.time.length - 23
        expect(options.length).toBe(range)
      })
    })
    it('displays correct amount over multiple modals', () => {
      wrapper.destroy()
      wrapper = mount(DayModal, {
        propsData: {
          name: 'mon' + time[23],
          index: 23
        }
      })
      expect(wrapper.vm.selected).toBe('')
      wrapper.setData({ selected: time[23] })
      wrapper.trigger('beforeClose')

      expect(wrapper.vm.optionsRange.length).toBe(
        // don't include self in optionsRange
        wrapper.vm.time.length - 23 - 1
      )
    })
    describe('toggleElements', () => {
      it('is triggered when beforeClose emitted', () => {
        wrapper.destroy()
        wrapper = mount(DayModal, {
          propsData: {
            name: 'mon' + time[23],
            index: 23
          }
        })

        let fakeDoc = {
          getElementById(name) {
            return { name, checked: false }
          }
        }
        let endOfRange, range
        endOfRange = 43
        range = time.slice(24, endOfRange + 1)
        let toggled = toggleElements(range, 'mon', fakeDoc)
        expect(toggled.length).toBeTruthy()
        expect(toggled[0].name).toBe('mon' + time[24])
      })
    })
  })
})
