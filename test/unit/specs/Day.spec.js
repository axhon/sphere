import { shallow } from 'vue-test-utils'
import Day from '@/components/Day'

describe('Day.vue', () => {
  it('should have props', () => {
    let wrapper = shallow(Day)
    expect(typeof wrapper.props()).toBe('object')
  })
  describe('weekend prop', () => {
    it('should be false', () => {
      let wrapper = shallow(Day)
      wrapper.setProps({ weekend: false })
      expect(wrapper.props().weekend).toBe(false)
    })
  })
})
