import { shallow } from 'vue-test-utils'
import App from '@/App'

describe('testing main App component', () => {
  it('should have a Day component', () => {
    let wrapper = shallow(App)
    expect(typeof wrapper.vm.$options.components.DayColumn).toBe('object')
  })
})
