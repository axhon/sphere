# Issue

### This calendar is responsive, but on touch devices, it doesn't allow to drag and select multiple checkboxes as on the desktop version.

# Assignment

Using the code on that page as your base, write a script that brings the user to a menu when one of the checkboxes is tapped on a touch device.

## **The Menu Should**

* give the user the ability to select
  * start time
  * an end time,
* and when the user is done with the menu
  * the checkboxes in the calendar should be selected based on the user's inputs.

_Users should be able to repeat this process to select as many blocks of checkboxes as they want._

## While adding this functionality

**make sure to follow progressive enhancement and that none of the existing functionalities are broken**
